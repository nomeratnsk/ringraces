package ru.nomerat.ringraces.service;

import org.springframework.test.util.ReflectionTestUtils;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.nomerat.ringraces.model.Track;
import ru.nomerat.ringraces.model.Transport;
import ru.nomerat.ringraces.service.loaders.CarsLoader;
import ru.nomerat.ringraces.service.loaders.MotorcyclesLoader;
import ru.nomerat.ringraces.service.loaders.TransportLoader;
import ru.nomerat.ringraces.service.loaders.TruckLoader;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class LoadDataServiceTest {

    private final String path = "./src/test/resources/tests/";

    private LoadDataService uut;

    @BeforeClass
    public void init() {
        List<TransportLoader> loaders = new ArrayList<>();
        CarsLoader carsLoader = new CarsLoader();
        loaders.add(carsLoader);
        MotorcyclesLoader motorcyclesLoader = new MotorcyclesLoader();
        loaders.add(motorcyclesLoader);
        TruckLoader truckLoader = new TruckLoader();
        loaders.add(truckLoader);
        uut = new LoadDataService(loaders);
        ReflectionTestUtils.setField(carsLoader, "path", path);
        ReflectionTestUtils.setField(motorcyclesLoader, "path", path);
        ReflectionTestUtils.setField(truckLoader, "path", path);
        ReflectionTestUtils.setField(uut, "path", path);
    }

    @Test
    public void testLoadTrackSuccess()  {
        Track expected = new Track();
        expected.setLenght(600);
        Track track = uut.loadTrek();
        assertThat(expected.getLenght()).isEqualTo(track.getLenght());
    }

    @Test
    public void testLoadTransportSuccess()  {
        List<Transport> transports = uut.loadTransport();
        System.out.println(transports);
        assertThat(transports.size()).isEqualTo(3);
    }

}
