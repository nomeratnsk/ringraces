package ru.nomerat.ringraces;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RingracesApplication {

    public static void main(String[] args) {
        SpringApplication.run(RingracesApplication.class, args);
    }
}
