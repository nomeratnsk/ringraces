package ru.nomerat.ringraces.service.loaders;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ru.nomerat.ringraces.model.Car;
import ru.nomerat.ringraces.model.Transport;
import ru.nomerat.ringraces.utils.JsonUtils;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Slf4j
@Component
public class CarsLoader implements TransportLoader {

    @Value("${file.data.source.path}")
    private String path;

    @Override
    public List<Transport> load() {
        try {
            return Arrays.asList(JsonUtils.readJson(getFilePath("cars.json"), Car[].class));
        } catch (IOException e) {
            log.error("Не удалось прочитать файл с автомобилями. Ошибка: " + e.getMessage());
        }
        return Collections.emptyList();
    }

    private String getFilePath(String fileName) {
        return path + fileName;
    }
}
