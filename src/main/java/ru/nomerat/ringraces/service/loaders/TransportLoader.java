package ru.nomerat.ringraces.service.loaders;

import ru.nomerat.ringraces.model.Transport;

import java.util.List;

public interface TransportLoader {
    List<Transport> load();
}
