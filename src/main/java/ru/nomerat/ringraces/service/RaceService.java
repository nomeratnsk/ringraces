package ru.nomerat.ringraces.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.nomerat.ringraces.component.Race;
import ru.nomerat.ringraces.model.ListTransports;
import ru.nomerat.ringraces.model.Message;
import ru.nomerat.ringraces.model.Track;
import ru.nomerat.ringraces.model.Transport;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

@Slf4j
@Service
public class RaceService {


    public boolean startRace(Track track, ListTransports transports, ConcurrentLinkedQueue<Message> messages)  {

        int countRacers = transports.getItems().size();
        ExecutorService executorService = Executors.newFixedThreadPool(countRacers);
        List<Future<String>> list = new ArrayList<>();

        for (Transport transport : transports.getItems()) {
            Callable<String> callable = new Race(track, transport, messages);
            Future<String> future = executorService.submit(callable);
            list.add(future);
        }

        executorService.shutdown();

        return true;
    }

}
