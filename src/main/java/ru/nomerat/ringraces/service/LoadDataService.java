package ru.nomerat.ringraces.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.nomerat.ringraces.service.loaders.TransportLoader;
import ru.nomerat.ringraces.utils.JsonUtils;
import ru.nomerat.ringraces.model.Track;
import ru.nomerat.ringraces.model.Transport;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class LoadDataService {

    @Value("${file.data.source.path}")
    private String path;

    private final List<TransportLoader> loaders;

    @Autowired
    public LoadDataService(List<TransportLoader> loaders) {
        this.loaders = loaders;
    }

    public Track loadTrek() {

        try {
            return JsonUtils.readJson(getFilePath("track.json"), Track.class);
        } catch (IOException e) {
            log.error("Не удалось прочитать файл с описанием трека. Ошибка: " + e.getMessage());
        }
        return null;
    }

    public List<Transport> loadTransport() {
        List<Transport> transports = new ArrayList<>();
        for (TransportLoader loader : loaders) {
            transports.addAll(loader.load());
        }
        return transports;
    }

    private String getFilePath(String fileName) {
        return path + fileName;
    }

}
