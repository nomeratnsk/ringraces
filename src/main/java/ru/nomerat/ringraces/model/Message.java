package ru.nomerat.ringraces.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class Message {

    public enum Status {
        READY ("Готов!"),
        WAY ("В пути!"),
        PUNCTURE ("Прокол!"),
        FINISH ("Финиш!");

        private String label;

        Status(String label) {
            this.label = label;
        }
        public String getLabel() {
            return this.label;
        }
    }

    private LocalDateTime dateTime;
    private String name;
    private Status status;
    private String special;

    public Message(String name, Status status, String special) {
        this.dateTime = LocalDateTime.now();
        this.name = name;
        this.status = status;
        this.special = special;
    }

    @Override
    public String toString() {
        return this.dateTime + " : " + this.name + " : " + this.status.label + " : " + this.special;
    }
}
