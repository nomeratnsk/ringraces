package ru.nomerat.ringraces.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class Car extends Transport {

    private int passengers;

    public Car() {
        this.setType(Type.CAR);
    }

    protected String getSpecial() {
        if (this.passengers == 0) {
            return "без пассажиров.";
        }
        return "везу пассажиров: " + passengers;
    }
}
