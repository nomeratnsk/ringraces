package ru.nomerat.ringraces.model;

import java.util.Queue;

public interface Reporting<T extends Message> {

    void sendReportAboutParameters(Queue<T> queue);
    void sendReportAboutDistance(Queue<T> queue);
    void sendReportAboutPuncture(Queue<T> queue);
    void sendReportAboutFinish(Queue<T> queue);
}
