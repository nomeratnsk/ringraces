package ru.nomerat.ringraces.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.Queue;

@Getter
@Setter
public abstract class Transport implements Reporting<Message> {

    public enum Type {
        CAR ("Автомобиль"),
        MOTORCYCLE ("Мотоцикл"),
        TRUCK ("Грузовик");

        private String label;

        Type(String label) {
            this.label = label;
        }

        public String getLabel() {
            return this.label;
        }
    }

    private Type type;
    private String name;
    private int speed;
    private int distance;
    private int puncture;
    private LocalDateTime finish;

    public void sendReportAboutParameters(Queue<Message> messageQueue) {
        messageQueue.add(new Message(this.name, Message.Status.READY, getParameters()));
    }

    public void sendReportAboutDistance(Queue<Message> messageQueue) {
        messageQueue.add(new Message(this.name, Message.Status.WAY, getDistance()));
    }

    public void sendReportAboutPuncture(Queue<Message> messageQueue) {
        messageQueue.add(new Message(this.name, Message.Status.PUNCTURE, "Ай-ай..."));
    }

    public void sendReportAboutFinish(Queue<Message> messageQueue) {
        messageQueue.add(new Message(this.name, Message.Status.FINISH, "Ура!"));
    }

    private String getDistance() {
        return "прошёл " + this.distance;
    }

    public String getParameters() {
        return "скорость = " + this.speed + ", вероятность прокола = " + this.puncture + ", " +  getSpecial();
    }

    public String getFeature() {
        return getSpecial();
    }

    protected abstract String getSpecial();

    public String toString() {
        return this.name + " (скорость = " + this.speed + ", вероятность прокола = " + this.puncture + ", " +  getSpecial() +" )";
    }
}
