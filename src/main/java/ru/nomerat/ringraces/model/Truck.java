package ru.nomerat.ringraces.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class Truck extends Transport {

    private int cargo;

    public Truck() {
        this.setType(Type.TRUCK);
    }

    protected String getSpecial() {
        if (this.cargo == 0) {
            return "без груза.";
        }
        return "на борту груз: " + cargo;
    }
}
