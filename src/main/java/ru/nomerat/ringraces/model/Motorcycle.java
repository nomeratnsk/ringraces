package ru.nomerat.ringraces.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class Motorcycle extends Transport {

    private boolean sidecar;

    public Motorcycle() {
        this.setType(Type.MOTORCYCLE);
    }

    protected String getSpecial() {
        if (sidecar) {
            return "с коляской.";
        }
        return "без коляски";
    }
}
