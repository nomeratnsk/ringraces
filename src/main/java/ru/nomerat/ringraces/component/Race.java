package ru.nomerat.ringraces.component;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import ru.nomerat.ringraces.model.Message;
import ru.nomerat.ringraces.model.Track;
import ru.nomerat.ringraces.model.Transport;

import java.util.Queue;
import java.util.concurrent.Callable;

@Slf4j
@Getter
@Setter
@AllArgsConstructor
public class Race implements Callable<String> {

    private Track track;
    private Transport transport;
    private Queue<Message> messages;

    @Override
    public String call() throws Exception {

        // вычисляем время гонки
        double seconds = 1.0 * track.getLenght()/transport.getSpeed();
        int segments = (int) seconds;
        double rest = seconds - segments;
        // вычисляем, будет ли прокол
        boolean puncture = ((int)(Math.random() * 100)) < transport.getPuncture();
        // если будет прокол вычисляем отрезок
        int segment = -1;
        if (puncture) {
            segment = ((int)(Math.random() * segments));
        }

        transport.sendReportAboutParameters(messages);
        for (int i = 0; i < segments; i++) {
            Thread.sleep(1000);
            int way = (i+1) * transport.getSpeed();
            transport.setDistance(way);
            transport.sendReportAboutDistance(messages);
            if (i == segment) {
                transport.sendReportAboutPuncture(messages);
                Thread.sleep(1000);
            }
        }
        Thread.sleep((long)(rest * 1000));
        transport.sendReportAboutFinish(messages);

        return "";
    }
}
