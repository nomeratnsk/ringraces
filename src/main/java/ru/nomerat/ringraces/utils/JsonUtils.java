package ru.nomerat.ringraces.utils;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;

public class JsonUtils {

    protected static final ObjectMapper OBJECT_MAPPER;

    static {
        OBJECT_MAPPER = new ObjectMapper();
        OBJECT_MAPPER.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
    }

    public static <T> T readJson(String fileName, Class<T> clazz)
            throws IOException {
        return OBJECT_MAPPER.readValue(new File(fileName), clazz);
    }

    public static <T> String writeValueAsString(T actual) throws Exception {
        return OBJECT_MAPPER.writeValueAsString(actual);
    }

}
