package ru.nomerat.ringraces.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import ru.nomerat.ringraces.model.ListTransports;
import ru.nomerat.ringraces.model.Message;
import ru.nomerat.ringraces.model.Track;
import ru.nomerat.ringraces.service.LoadDataService;
import ru.nomerat.ringraces.service.RaceService;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.stream.Collectors;

@Slf4j
@Controller
@SessionAttributes(value = {"track", "transports", "information"})
public class RingRacesController {

    private final LoadDataService loadDataService;
    private final RaceService raceService;

    @Autowired
    public RingRacesController(LoadDataService loadDataService,
                               RaceService raceService) {
        this.loadDataService = loadDataService;
        this.raceService = raceService;
    }

    @ModelAttribute("track")
    public Track createTrack() {
        return new Track();
    }

    @ModelAttribute("transports")
    public ListTransports createListTransports() {
        return new ListTransports();
    }

    @ModelAttribute("information")
    public ConcurrentLinkedQueue<Message> createInfoPanel() {
        return new ConcurrentLinkedQueue<>();
    }

    @GetMapping({"/", "/index"})
    public String index(SessionStatus sessionStatus) {
        sessionStatus.setComplete();
        return "index";
    }

    @GetMapping("/transports")
    public String transports(@ModelAttribute("transports") ListTransports transports,
                             @ModelAttribute("track") Track track,
                             Model model) {

        transports.setItems(loadDataService.loadTransport());
        track = loadDataService.loadTrek();

        log.info("Длина трека: " + track.getLenght());
        log.info("Количество участников: " + transports.getItems().size());

        model.addAttribute("track", track);
        model.addAttribute("transports", transports);

        return "transports";
    }

    @PostMapping("/racing")
    public String racing(@ModelAttribute("track") Track track,
                         @ModelAttribute("transports") ListTransports transports,
                         @ModelAttribute("information") ConcurrentLinkedQueue<Message> messages,
                         Model model) {
        log.info("Старт гонки!");
        messages.clear();
        raceService.startRace(track, transports, messages);
        return "redirect:/info";
    }

    @GetMapping("/info")
    public String info(@ModelAttribute("track") Track track,
                       @ModelAttribute("transports") ListTransports transports,
                       @ModelAttribute("information") ConcurrentLinkedQueue<Message> messages,
                       Model model) {

        List<Message> history = new ArrayList<>(messages);
        model.addAttribute("history", history);

        return "info";
    }

    @GetMapping("/result")
    public String result(@ModelAttribute("track") Track track,
                       @ModelAttribute("transports") ListTransports transports,
                       @ModelAttribute("information") ConcurrentLinkedQueue<Message> messages,
                       Model model) {

        List<Message> history = new ArrayList<>(messages);
        model.addAttribute("history", history);

        long finish = history.stream()
                .map(Message::getStatus)
                .filter(status -> status == Message.Status.FINISH)
                .count();

        if (finish != transports.getItems().size()) {
            return "redirect:/info";
        }

        List<Message> result = history.stream()
                .filter(message -> message.getStatus() == Message.Status.FINISH)
                .sorted(Comparator.comparing(Message::getDateTime))
                .collect(Collectors.toList());

        log.info("Результат гонки: {}", result);

        model.addAttribute("result", result);

        return "result";
    }
}
